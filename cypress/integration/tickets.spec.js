describe("Tickets", ()=> {

beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html") )

it("Has 'TICKETBOX` Headers's heading", () => {
    cy.get("header h1").should("contain", "TICKETBOX" );
})

it("fills all the text input fields", () => {
    const firstName = "Rafael";
    const lastName = "Calandreli";
    //Comando Get para pegar o elemento e type para preenche-lo
    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("rcalandreli@gmail.com");
    cy.get("#requests").type("Vegetarian");
    cy.get("#signature").type(`${firstName} ${lastName}`);

})

it("Select two tickets", () => {
   cy.get("#ticket-quantity").select("2");
})

it("Select Vip ticket type", () => {
    cy.get("#vip").check();
})

it("Select All Checkbox", () => {
    cy.get("#friend").check();
    cy.get("#publication").check();
    cy.get("#social-media").check();
})

it("alerts on invalid email", () => {
    cy.get("#email")
      .as("email")
      .type("rcalandreli-gmail.com");
    
    cy.get("#email.invalid")
       .should("exist");

    cy.get("@email")
        .clear()
        .type("rcalandreli@gmail.com");

    cy.get("#email.invalid")
        .should("not.exist");    
})

it("Register with sucessfuly", () => {
    const firstName = "Rafael";
    const lastName = "Calandreli";
    const fullName = `${firstName} ${lastName}`;
    
    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("rcalandreli@gmail.com"); 
    cy.get("#ticket-quantity").select("2");
    cy.get("#vip").check();
    cy.get("#friend").check();
    cy.get("#requests").type("IPA beer");
    //Validation
    cy.get(".agreement p").should(
        "contain", `I, ${fullName}, wish to buy 2 VIP tickets.`);

    cy.get("#agree").check();    
    cy.get("#signature").type(fullName);

    cy.get("button[type='submit']")
       .as("submitButton")
       .should("not.be.disabled")

    cy.get("@submitButton").click();
    
    cy.get(".success > p")
        .should("exist");
  })

  it("Reset all fills with sucessfuly", () => {
    const firstName = "Rafael";
    const lastName = "Calandreli";
    const fullName = `${firstName} ${lastName}`;
    
    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("rcalandreli@gmail.com"); 

    cy.get("#ticket-quantity").select("2");
    cy.get("#vip").check();
    cy.get("#friend").check();
    cy.get("#requests").type("IPA beer");
    //Validation
    cy.get(".agreement p").should(
        "contain", `I, ${fullName}, wish to buy 2 VIP tickets.`);
    
    cy.get("#agree").check();    
    cy.get("#signature").type(fullName);

    cy.get("button[type='submit']")
       .as("submitButton")
       .should("not.be.disabled")

    cy.get("button[type='reset']").click();

    cy.get("@submitButton").should("be.disabled");    
  })

  it("Fills mandatory fields using support command", () => {
    const customer = {
        firstName: "João",
        lastName: "Silva",
        email: "joaosilva@example.com"
    };

    cy.fillMandatoryFields(customer);

    cy.get("button[type='submit']")
       .as("submitButton")
       .should("not.be.disabled")

    cy.get("#agree").uncheck();

    cy.get("@submitButton").should("be.disabled");   
       


  })

});